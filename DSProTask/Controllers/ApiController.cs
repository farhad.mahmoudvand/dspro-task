﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DSProTask.Controllers
{
    public class ApiController : Controller
    {

        public JsonResult GetProviders(string keyword, int page, int sortBy, bool sortAsce)
        {
            try
            {
                using (var db = new DataLayer.dsproEntities())
                {
                    var data = db.Providers.AsQueryable();
                    var size = 10;
                    page = page - 1;
                    var result = new Models.ProvidersResult();
                    
                    if(keyword != null && keyword != "")
                    {
                        data = data.Where(f => f.ProviderName.Contains(keyword));
                    }

                    if (sortAsce) data = sortBy == 1 ? data.OrderBy(f => f.ProviderName)
                             : sortBy == 2 ? data.OrderBy(f => f.SaleId)
                             : sortBy == 3 ? data.OrderBy(f => f.CreateDate)
                             : data.OrderBy(f => f.Id);
                    else data = sortBy == 1 ? data.OrderByDescending(f => f.ProviderName)
                             : sortBy == 2 ? data.OrderByDescending(f => f.SaleId)
                             : sortBy == 3 ? data.OrderByDescending(f => f.CreateDate)
                             : data.OrderByDescending(f => f.Id);

                    var c = data.Count();
                    result.PagesCount = c / size;
                    if (c % size > 0) result.PagesCount++;
                    result.Providers = data.Skip(page * size).Take(size).ToList()
                        .Select(f => new Models.ProviderItem
                        {
                            Id = f.Id,
                            ProviderName = f.ProviderName,
                            SaleId = f.SaleId,
                            CreateDate = f.CreateDate.ToString("dd MMM yyyy"),
                            EditMode = false
                        }).ToList();
                    return CreateJsonResult(result);
                }
            }
            catch (Exception ex)
            {
                return CreateErrorResult(ex.Message);
            }
        }

        public JsonResult SaveProvider(int id, string providerName, int saleId)
        {
            try
            {
                using(var db = new DataLayer.dsproEntities())
                {
                    var provider = new DataLayer.Provider();
                    if(id > 0) provider = db.Providers.Find(id);
                    provider.ProviderName = providerName;
                    provider.SaleId = saleId;
                    if (id == 0)
                    {
                        provider.CreateDate = DateTime.Now;
                        db.Providers.Add(provider);
                    }
                    db.SaveChanges();
                    return CreateJsonResult(new Models.ProviderItem
                    {
                        Id = provider.Id,
                        CreateDate = provider.CreateDate.ToString("dd MMM yyyy"),
                        EditMode = false,
                        ProviderName = provider.ProviderName,
                        SaleId = provider.SaleId
                    });
                }
            }
            catch (Exception ex)
            {
                return CreateErrorResult(ex.Message);
            }
        }

        public JsonResult DeleteProvider(int id)
        {
            try
            {
                using (var db = new DataLayer.dsproEntities())
                {
                    var provider = db.Providers.Find(id);
                    db.Providers.Remove(provider);
                    db.SaveChanges();
                    return CreateJsonResult(true);
                }
            }
            catch (Exception ex)
            {
                return CreateErrorResult(ex.Message);
            }
        }


        private JsonResult CreateJsonResult(object data)
        {
            return Json(new Models.JsonResult
            {
                data = data,
                success = true,
                message = ""
            });
        }

        private JsonResult CreateJsonResult(object data, string message)
        {
            return Json(new Models.JsonResult
            {
                data = data,
                success = true,
                message = message
            });
        }

        private JsonResult CreateErrorResult(string error)
        {
            return Json(new Models.JsonResult
            {
                data = null,
                success = false,
                message = error
            });
        }


    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DSProTask.Models
{
    public class JsonResult
    {
        public object data { get; set; }
        public bool success { get; set; }
        public string message { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DSProTask.Models
{
    public class ProviderItem
    {
        public int Id { get; set; }
        public string ProviderName { get; set; }
        public int SaleId { get; set; }
        public string CreateDate { get; set; }
        public bool EditMode { get; set; }
    }
}
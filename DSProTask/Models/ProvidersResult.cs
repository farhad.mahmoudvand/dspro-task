﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DSProTask.Models
{
    public class ProvidersResult
    {
        public int PagesCount { get; set; } 
        public List<ProviderItem> Providers { get; set; }
    }
}
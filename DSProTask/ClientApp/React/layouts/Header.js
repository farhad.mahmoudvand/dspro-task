﻿import React, { Component } from "react";
import { connect } from "react-redux";

class Header extends Component {
    constructor(props) {
        super(props);

    }

    render() {
        return (
            <header>
                <div className="container">
                    <div className="row">
                        <div className="col col-12">
                            <h2>
                                DSPro Data Grid Task
                            </h2>
                            <p>By Farhad Mahmoudvand</p>
                        </div>
                    </div>
                </div>
            </header>
        );
    }
}

const mapStateProps = state => {
    return {
        
    };
};

const mapDispatchProps = dispatch => {
    return {
        
    };
};


export default connect(
    mapStateProps,
    mapDispatchProps
)(Header);
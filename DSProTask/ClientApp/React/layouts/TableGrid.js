﻿import React, { Component } from "react";
import { connect } from "react-redux";
import axios from "axios";
import { Button, Modal } from 'react-bootstrap';
import * as actions from "../redux/actions";
import "../styles/tablegrid.css";

class TableGrid extends Component {
    constructor(props) {
        super(props);
        this.state = {
            errorModal: {
                show: false,
                message: ''
            },
            deleteModal: 0,
            createRow: true
        };

        this.goToPage = this.goToPage.bind(this);
        this.filterData = this.filterData.bind(this);
        this.saveRow = this.saveRow.bind(this);
        this.closeErrorModal = this.closeErrorModal.bind(this);
        this.deleteRow = this.deleteRow.bind(this);
        this.deleteConfirm = this.deleteConfirm.bind(this);
        this.closeDeleteModal = this.closeDeleteModal.bind(this);
    }

    componentDidMount() {
        this.props.loadData();
    }

    closeErrorModal() {
        this.setState({
            errorModal: {
                show: false,
                message: ''
            },
            deleteModal: {
                show: false,
                id: 0
            }
        });
    }

    filterData(e) {
        e.preventDefault();
        const formData = new FormData(e.target);
        this.props.loadData(1, formData.get("keyword"),
            Number.parseInt(formData.get("sortBy")),
            Number.parseInt(formData.get("sortOrder")));
        
    }

    goToPage(p) {
        this.props.loadData(p);
    }

    saveRow(e) {
        e.preventDefault();
        const formData = new FormData(e.target);
        var rowId = Number.parseInt(formData.get("edit_id"));
        var providerName = formData.get("provider_name");
        var saleId = Number.parseInt(formData.get("sale_id"));
        if (isNaN(saleId) || saleId === 0) {
            this.setState({
                errorModal: {
                    show: true,
                    message: 'Please enter a valid number for Sale Id!'
                }
            });
            return;
        }
        this.props.saveRow(rowId, providerName, saleId, this.state.createRow);
        this.setState({
            createRow: true
        });

        e.target.reset();
    }

    deleteRow(id) {
        this.setState({
            deleteModal: {
                show: true,
                id: id
            }
        });
    }
    deleteConfirm() {
        this.props.deleteRow(this.state.deleteModal.id);
        this.closeDeleteModal();
    }
    closeDeleteModal() {
        this.setState({
            deleteModal: {
                show: false,
                id: 0
            }
        });
    }


    render() {
        return (
            <>
                <div className="container">
                
                    <div className="row">
                        <div className="col col-12">
                            {this.state.title}
                            <div className="card card-filter">
                                <div className="card-body">
                                    <form onSubmit={this.filterData}>
                                        <div className="row">
                                            <div className="col">
                                                <div>Keyword: </div>
                                                <input name="keyword" type="text" className="form-control" placeholder="Type a keyword..." />
                                            </div>
                                            <div className="col">
                                                <div>Sort By: </div>
                                                <select name="sortBy" className="form-control">
                                                    <option value="0">Id</option>
                                                    <option value="1">Provider Name</option>
                                                    <option value="2">Sale Id</option>
                                                    <option value="3">Date</option>
                                                </select>
                                            </div>
                                            <div className="col">
                                                <div>Sort Order: </div>
                                                <select name="sortOrder" className="form-control">
                                                    <option value="1">Ascending</option>
                                                    <option value="0">Descending</option>
                                                </select>
                                            </div>
                                            <div className="col">
                                                <div>&nbsp;</div>
                                                <button type="submit" className="btn btn-primary">
                                                    Submit
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col col-12 col-data-grid">
                            <div className="table-responsive">
                                <form onSubmit={this.saveRow}>
                                    <table className="table">
                                        <thead>
                                            <tr>
                                                <th>
                                                    Id
                                                </th>
                                                <th>
                                                    Provider Name
                                                </th>
                                                <th>
                                                    Sale Id
                                                </th>
                                                <th>
                                                    Create Date
                                                </th>
                                                <th>
                                                    Actions
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {
                                                this.props.providersData.data.map((item, key) => 
                                                    item.EditMode ?
                                                        <tr key={key}>
                                                            <td>
                                                                {item.Id}
                                                                <input type="hidden" name="edit_id" value={item.Id} />
                                                            </td>
                                                            <td>
                                                                <input type="text" name="provider_name" className="form-control" defaultValue={item.ProviderName} autoFocus required />
                                                            </td>
                                                            <td>
                                                                <input type="text" name="sale_id" className="form-control" defaultValue={item.SaleId} required />
                                                            </td>
                                                            <td>
                                                                {item.CreateDate}
                                                            </td>
                                                            <td>
                                                                <button type="submit" className="btn btn-success">
                                                                    Update
                                                                </button>
                                                            </td>
                                                        </tr>
                                                        :
                                                        <tr key={key}>
                                                            <td>
                                                                {item.Id}
                                                            </td>
                                                            <td>
                                                                {item.ProviderName}
                                                            </td>
                                                            <td>
                                                                {item.SaleId}
                                                            </td>
                                                            <td>
                                                                {item.CreateDate}
                                                            </td>
                                                            <td>
                                                                <button onClick={(e) => { e.preventDefault(); this.props.editRow(item.Id); this.setState({ createRow: false }); }} type="button" className="btn btn-info btn-sm" title="Edit">
                                                                    <i className="far fa-edit"></i>
                                                                </button>
                                                                &nbsp;
                                                                <button onClick={(e) => { e.preventDefault(); this.deleteRow(item.Id); }} type="button" className="btn btn-danger btn-sm" title="Delete">
                                                                    <i className="fas fa-trash"></i>
                                                                </button>
                                                            </td>
                                                        </tr>
                                                )
                                            }
                                            {
                                                this.state.createRow &&
                                                <tr>
                                                    <td>
                                                        <input type="hidden" name="edit_id" value="0" />
                                                    </td>
                                                    <td>
                                                        <input type="text" name="provider_name" className="form-control" required />
                                                    </td>
                                                    <td>
                                                        <input type="text" name="sale_id" className="form-control" required />
                                                    </td>
                                                    <td>
                                                        
                                                    </td>
                                                    <td>
                                                        <button type="submit" className="btn btn-success">
                                                            Create Row
                                                        </button>
                                                    </td>
                                                </tr>
                                            }
                                        </tbody>
                                     </table>
                                </form>
                            </div>
                            <div className="row">
                                <div className="col">
                                    <ul className="pagination">
                                        {
                                            this.props.providersData.pages.map((page, key) =>
                                                <li key={key} className={this.props.providersData.page === page ? "page-item active" : "page-item"}><a href="javascript:;" className="page-link" onClick={() => this.goToPage(page)}>{page}</a></li>
                                            )
                                        }
                                    </ul>
                                </div>
                            </div>
                            {
                                this.props.providersData.isLoading
                                &&
                                <div className="table-loader">
                                    <div className="table-loader-content">
                                        Loading...
                                    </div>
                                </div>
                            }
                        </div>
                    </div>
                </div>

                <Modal show={this.state.errorModal.show}>
                    <Modal.Body>
                        <p>
                            {this.state.errorModal.message}
                        </p>
                    </Modal.Body>
                        <Modal.Footer>
                            <Button variant="danger" onClick={this.closeErrorModal}>
                                Ok
                            </Button>
                    </Modal.Footer>
                </Modal>

                <Modal show={this.state.deleteModal.show}>
                    <Modal.Body>
                        <p>
                            Are you sure you want to delete this row?
                        </p>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="danger" onClick={this.deleteConfirm} >
                            Confirm & Delete
                        </Button>
                        <Button variant="secondary" onClick={this.closeDeleteModal}>
                            Cancel
                        </Button>
                    </Modal.Footer>
                </Modal>
            </>
            );
    }
}

const mapStateProps = state => {
    return {
        providersData: state.providersData
    };
};

const mapDispatchProps = dispatch => {
    return {
        loadData: async (page = 1, keyword = "", sortBy = 0, sortAsce = 1) => {
            dispatch(actions.loadProviders(page));
            await axios
                .post("/api/GetProviders", {
                    keyword: keyword,
                    page: page,
                    sortBy: sortBy,
                    sortAsce: sortAsce
                })
                .then(res => {
                    dispatch(actions.bindProviders(res.data.data));
                })
                .catch(err => {
                    console.log(err);
                });
        },
        editRow: async id => {
            dispatch(actions.editProvider(id));
        },
        saveRow: async (id, name, saleId, create) => {
            await axios
                .post("/api/SaveProvider", {
                    id: id,
                    providerName: name,
                    saleId: saleId
                })
                .then(res => {
                    if (create) {
                        dispatch(actions.addProvider(res.data.data));
                    }
                    else {
                        dispatch(actions.updateProvider({
                            id: id,
                            providerName: name,
                            saleId: saleId
                        }));
                    }
                })
                .catch(err => {
                    console.log(err);
                });
        },
        deleteRow: async (id) => {
            await axios
                .post("/api/DeleteProvider", {
                    id: id
                })
                .then(res => {
                    dispatch(actions.deleteProvider(id));
                })
                .catch(err => {
                    console.log(err);
                });
        }
    };
};

export default connect(
    mapStateProps,
    mapDispatchProps
)(TableGrid);
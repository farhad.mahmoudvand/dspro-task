﻿import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import * as Layouts from './layouts/AllLayouts';
import store from './redux/store';
import './App.css';

if (document.getElementById('app')) {
    ReactDOM.render(
        <Provider store={store}>
            <Layouts.Header />
            <Layouts.TableGrid />
        </Provider>
        , document.getElementById('app'));
}
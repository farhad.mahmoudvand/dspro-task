﻿import { InitialState } from "./initialState";
export default function rootReducer(state = InitialState, action) {
    if (action.type === "LOAD_PROVIDERS") {
        return {
            ...state,
            providersData: {
                ...state.providersData,
                isLoading: true,
                page: action.page
            }
        };
    }
    else if (action.type === "BIND_PROVIDERS") {
        let pages = [];
        for (let p = 0; p < action.data.PagesCount; p++) pages.push(p + 1);
        return {
            ...state,
            providersData: {
                ...state.providersData,
                isLoading: false,
                data: action.data.Providers,
                pages: pages
            }
        };
    }
    else if (action.type === "EDIT_PROVIDER") {
        let data = state.providersData.data;
        for (let i = 0; i < data.length; i++) data[i].EditMode = data[i].Id === action.id;
        return {
            ...state,
            providersData: {
                ...state.providersData,
                data: data
            }
        };
    }
    else if (action.type === "UPDATE_PROVIDER") {
        let data = state.providersData.data;
        for (let i = 0; i < data.length; i++) {
            data[i].EditMode = false;
            if (data[i].Id === action.data.id) {
                data[i].ProviderName = action.data.providerName;
                data[i].SaleId = action.data.saleId;
            }
        }
        return {
            ...state,
            providersData: {
                ...state.providersData,
                data: data
            }
        };
    }
    else if (action.type === "ADD_PROVIDER") {
        let data = state.providersData.data;
        data.push(action.data);
        return {
            ...state,
            providersData: {
                ...state.providersData,
                data: data
            }
        };
    }
    else if (action.type === "DELETE_PROVIDER") {
        let data = state.providersData.data.filter(function (row) {
            return row.Id !== action.id;
        });
        
        return {
            ...state,
            providersData: {
                ...state.providersData,
                data: data
            }
        };
    }
    else return state;

}

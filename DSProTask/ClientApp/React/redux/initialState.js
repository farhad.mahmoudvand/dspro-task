﻿export const InitialState = {
    appLoading: true,
    appLoaded: false,
    providersData: {
        isLoading: false,
        page: 1,
        pages: [],
        data: []
    }
};
﻿
export function loadProviders(page) {
    return {
        type: "LOAD_PROVIDERS",
        page: page
    };
}

export function bindProviders(data) {
    return {
        type: "BIND_PROVIDERS",
        data: data
    };
}

export function editProvider(id) {
    return {
        type: "EDIT_PROVIDER",
        id: id
    };
}

export function updateProvider(data) {
    return {
        type: "UPDATE_PROVIDER",
        data: data
    };
}

export function addProvider(data) {
    return {
        type: "ADD_PROVIDER",
        data: data
    };
}

export function deleteProvider(id) {
    return {
        type: "DELETE_PROVIDER",
        id: id
    };
}
